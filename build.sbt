/* basic project info */
name := "opendisc"

organization := "name.kaeding"

version := "0.1.0-SNAPSHOT"

description := "extract structured data from house disclosure PDF forms"

//homepage := Some(url("https://github.com/pkaeding/myproj"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.3"

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* entry point */
mainClass in (Compile, packageBin) := Some("name.kaeding.opendisc.Main")

mainClass in (Compile, run) := Some("name.kaeding.opendisc.Main")

/* dependencies */
libraryDependencies ++= Seq (
  "org.scalaz" %% "scalaz-core" % "7.0.5",
  "org.scalaz" %% "scalaz-effect" % "7.0.5",
  "org.clapper" %% "grizzled-slf4j" % "1.0.1",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "com.h2database" % "h2" % "1.3.175",
  "com.typesafe.slick" %% "slick" % "2.0.0",
  "org.scalacheck" %% "scalacheck" % "1.11.3" % "test",
  "org.specs2" %% "specs2" % "2.3.7" % "test",
  "com.sun.jna" % "jna" % "3.0.9",
  "net.java.dev.jai-imageio" % "jai-imageio-core-standalone" % "1.2-pre-dr-b04-2013-04-23"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository,
  "myGrid Repository" at "http://www.mygrid.org.uk/maven/repository"
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>patrick@kaeding.name</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

/* assembly plugin */
mainClass in AssemblyKeys.assembly := Some("name.kaeding.opendisc.Main")

assemblySettings

test in AssemblyKeys.assembly := {}

initialCommands in console := """
import scalaz._, Scalaz._
import scala.slick.driver.H2Driver.simple._
import name.kaeding.opendisc._
import name.kaeding.opendisc.db._
import model._
val database = Database.forURL("jdbc:h2:db", driver="org.h2.Driver")
implicit val session: Session = database.createSession
"""

cleanupCommands in console := """
session.close
"""