package name.kaeding.opendisc

import scalaz._, Scalaz._

import java.io.OutputStream
import java.net.URI

import img._
import javax.imageio.ImageIO

trait MFile[M[_]] {
  def saveFile(writeFn: OutputStream => Unit): M[URI]
}

object MFile {
  def saveFile[M[_]: Monad](writeFn: OutputStream => Unit)(implicit M: MFile[M]) =
  	M.saveFile(writeFn)

  def saveImage[M[_]: Monad: MFile](img: Image): M[URI] =
  	saveFile[M](os => ImageIO.write(img.toBufferedImage, "png", os))
}