package name.kaeding.opendisc

import scalaz._, Scalaz._

import img.forms._

trait MScannedForm[M[_]] {
  def saveSchedule3Electronic(form: Schedule3Electronic): M[Unit]
  def getSchedule3ElectronicById(id: Long): M[Option[Schedule3Electronic]]
}

object MScannedForm {
  def saveSchedule3Electronic[M[_]: Monad: MFile](form: Schedule3Electronic)(implicit M: MScannedForm[M]) =
    M.saveSchedule3Electronic(form)
    
  def getSchedule3ElectronicById[M[_]: Monad](id: Long)(implicit M: MScannedForm[M]) =
    M.getSchedule3ElectronicById(id)
}