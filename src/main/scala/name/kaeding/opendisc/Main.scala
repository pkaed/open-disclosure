package name.kaeding.opendisc

import scalaz._, Scalaz._, effect._

import javax.imageio.ImageIO
import javax.imageio.spi.IIORegistry
import java.io.File
import java.util.UUID
import java.awt.image._
import java.awt.geom.AffineTransform
import java.awt.color.ColorSpace
import java.awt.Color
import java.awt.geom.Line2D

import img._
import img.forms._
import db._, fs._

object Main extends SafeApp {
  import scala.slick.driver.H2Driver.simple._
  import slick.driver.H2Driver.profile
  import slick.driver._

  implicit val mfile = new MFileFilesystem(new File("/tmp"))
  implicit val mscannedform = new MScannedFormSql(new Tables(profile), Database.forURL("jdbc:h2:db;AUTO_SERVER=TRUE", driver="org.h2.Driver"))
  
  def initDb = IO(Database.forURL("jdbc:h2:db;AUTO_SERVER=TRUE", driver="org.h2.Driver").withSession { implicit s =>
    import db.Tables
    import profile.Implicit._
    val ddl: profile.SchemaDescription = mscannedform.tables.ddl.asInstanceOf[profile.SchemaDescription]
    ddl.drop
    ddl.create
  })

  def initImageIO = IO {
    ImageIO.scanForPlugins()
    IIORegistry.getDefaultInstance().registerApplicationClasspathSpis()
  }

  def readImage(file: String) = IO {
    val inputImage =
      ImageIO.read(ImageIO.createImageInputStream(new File(file)))
    preprocessPage(inputImage)
  }

  def identifyAndProcessPage(img: Image): IO[Schedule3Electronic] = {
    println(identifyFormName(img))
    // TODO do something with the parsed form name
    val form = processPage2(img)
    MScannedForm.saveSchedule3Electronic[IO](form) >> 
    IO.putStrLn(form.csv) >>
    form.point[IO]
  }

  def writeBoxesImage(img: BufferedImage, form: Schedule3Electronic): IO[Unit] = IO {
    import java.awt.{Color, Rectangle}
    val graphics = img.createGraphics()
    graphics.setColor(Color.blue)
    form.boxes.foreach(b =>
      graphics.draw(new Rectangle(b.minX, b.minY, b.width, b.height)))
    ImageIO.write(img, "png", new File("/tmp/boxes.png"))
  }
  
  override def run(args: ImmutableArray[String]): IO[Unit] = 
    for {
      _   <- initDb
      _   <- initImageIO
      biImg <- readImage(args(0))
      form  <- identifyAndProcessPage(biImg._2)
      _ <- writeBoxesImage(biImg._1, form)
    } yield ()
  
  def preprocessPage(page: BufferedImage): (BufferedImage, Image) = {
    val rotated = rotateLeft90degrees(page)
    ImageIO.write(rotated, "png", new File("/tmp/rotated.png"))
    val gray = toGrayscale(rotated)
    ImageIO.write(gray, "png", new File("/tmp/gray.png"))
    (rotated, Image(gray))
  }
  
  def identifyFormName(img: Image): Option[String] = 
    for {
      mainTopBorder <- img.findTopBorder(0.6)
      nameBoxTopBorder <- img.findTopBorder(0.2)
      trimmed = img.subImage(nameBoxTopBorder, img.maxX, mainTopBorder, img.minX)
      nameBoxLeftBorder <- trimmed.findLeftBorder(0.8)
      scheduleBoxWithBorder = trimmed.copy(maxX = nameBoxLeftBorder)
      scheduleBottom <- scheduleBoxWithBorder.findBottomBorder(0.6)
      scheduleRight <- scheduleBoxWithBorder.findRightBorder(0.4)
      scheduleBox = scheduleBoxWithBorder.subImage(scheduleBoxWithBorder.minY, scheduleRight, scheduleBottom, scheduleBoxWithBorder.minX)
      text <- scheduleBox.contract.ocr
    } yield text

  def processPage2(img: Image): Schedule3Electronic = {
    val id = UUID.randomUUID().toString()
    
    val topBorder: Option[Y] = img.findTopBorder(0.6)
    val bottomBorder: Option[Y] = 
      for {
        firstBottom <- img.findBottomBorder(0.6)
        nextBottom <- img.copy(maxY = firstBottom).findBottomBorder(0.6)
        textBetweenBottoms <- img.copy(minY = nextBottom, maxY = firstBottom).contract.ocr
      } yield if (textBetweenBottoms === "") 
        nextBottom 
      else 
        firstBottom

    val topBottomCropped = 
      ^(topBorder, bottomBorder)(img.subImage(_, img.maxX, _, img.minX)).getOrElse(img)

    val leftBorder: Option[X] = topBottomCropped.findLeftBorder(0.6)
    val rightBorder: Option[X] = topBottomCropped.findRightBorder(0.6)
    
    val cropped: Option[Image] = ^^^(topBorder, rightBorder, bottomBorder, leftBorder)(img.subImage)

    // Debug
    cropped.foreach(logHorizontalLineHistogram("/tmp/horiz-histogram", _))
    cropped.foreach(logVerticalLineHistogram("/tmp/histogram", _))

    cropped.flatMap(Schedule3Electronic(_)).get
  }

  def logResult(sch3: Schedule3Electronic) = {
    sch3.rows.zipWithIndex.foreach { r =>
      val (row, idx) = r
      val amt = row.amount.img.toBufferedImage
      val heldBy = row.heldBy.img.toBufferedImage
      val incomeType = row.incomeType.img.toBufferedImage
      val name = row.name.img.toBufferedImage
      val transaction = row.transaction.img.toBufferedImage
      val yearEndValue = row.yearEndValue.img.toBufferedImage
      println(s"row $idx")
      println(s"amount: ${row.amount.text}")
      println(s"heldBy: ${row.heldBy.text}")
      println(s"incomeType: ${row.incomeType.text}")
      println(s"name: ${row.name.text}")
      println(s"transaction: ${row.transaction.text}")
      println(s"yearEndValue: ${row.yearEndValue.text}")
      println("====")
      
      ImageIO.write(amt, "png", new File(s"/tmp/$id-row$idx-amount.png"))
      ImageIO.write(heldBy, "png", new File(s"/tmp/$id-row$idx-heldBy.png"))
      ImageIO.write(incomeType, "png", new File(s"/tmp/$id-row$idx-incomeType.png"))
      ImageIO.write(name, "png", new File(s"/tmp/$id-row$idx-name.png"))
      ImageIO.write(transaction, "png", new File(s"/tmp/$id-row$idx-transaction.png"))
      ImageIO.write(yearEndValue, "png", new File(s"/tmp/$id-row$idx-yearEndValue.png"))
    }
  }

  def rotateLeft90degrees(source: BufferedImage): BufferedImage = {
    val tx = new AffineTransform();
    tx.translate(source.getHeight() / 2, source.getWidth() / 2);
    tx.rotate(Math.toRadians(-90));
    tx.translate(-source.getWidth() / 2, -source.getHeight() / 2);
    val op = new AffineTransformOp(tx,
      AffineTransformOp.TYPE_BICUBIC);
    op.filter(source,
      new BufferedImage(source.getHeight(), source.getWidth(),
        BufferedImage.TYPE_BYTE_INDEXED));
  }

  def toGrayscale(source: BufferedImage): BufferedImage = {
    val op = new ColorConvertOp(
      ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
    op.filter(source,
      new BufferedImage(source.getWidth(), source.getHeight(),
        BufferedImage.TYPE_BYTE_GRAY));
  }


  // For logging historgrams:
  def toRGB(source: BufferedImage): BufferedImage = {
    val op = new ColorConvertOp(
      ColorSpace.getInstance(ColorSpace.CS_sRGB), null);
    op.filter(source,
      new BufferedImage(source.getWidth(), source.getHeight(),
        BufferedImage.TYPE_INT_RGB));
  }
  def countBlackPixelsInGroup[A](lines: Stream[(ImmutableArray[Pixel], A)]): (Int, A) = {
    val index = lines(math.round(lines.length.toFloat / 2))._2
    val rows = lines.map(_._1.toArray.toList).transpose
    val count = rows.count(r => Image.countBlackPixels(ImmutableArray.fromArray(r.toArray)) > 0)
    (count, index)
  }
  def logVerticalLineHistogram(path: String, img: Image) = {
    val counts = img.colsLeftToRight.sliding(3).map(countBlackPixelsInGroup[X])
    val bi = toRGB(img.toBufferedImage)
    val graphics = bi.createGraphics()
    graphics.setColor(Color.red)
    counts.foreach(c =>
      graphics.draw(new Line2D.Double(c._2 - img.minX, img.maxY - img.minY, c._2 - img.minX, img.maxY - img.minY - c._1)))
    ImageIO.write(bi, "png", new File(s"$path.png"))
  }
  def logHorizontalLineHistogram(path: String, img: Image) = {
    val counts = img.rowsTopToBottom.sliding(3).map(countBlackPixelsInGroup[Y])
    val bi = toRGB(img.toBufferedImage)
    val graphics = bi.createGraphics()
    graphics.setColor(Color.red)
    counts.foreach(c =>
      graphics.draw(new Line2D.Double(0, c._2 - img.minY, c._1, c._2 - img.minY)))
    ImageIO.write(bi, "png", new File(s"$path.png"))
  }
}
