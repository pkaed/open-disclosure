package name.kaeding.opendisc
package db

import scalaz._, Scalaz._, effect._

import slick.driver._
import JdbcDriver.simple._

import img._
import forms._

class MScannedFormSql(val tables: Tables, db: Database)(implicit mfile: MFile[IO]) extends MScannedForm[IO] {
  type M[_] = IO[_]
  import tables.profile.simple._
  import tables._
  import MFile._
  def saveSchedule3Electronic(form: Schedule3Electronic): IO[Unit] =
    for {
      imgFile <- saveImage[IO](form.img)
      formId  <- IO(db.withSession(implicit s => (schedule3Images returning schedule3Images.map(_.id)) += (-1, imgFile.toString)))
      _       <- form.rows.traverse(saveSchedule3ElectronicRow(formId))
    } yield ()

  private def saveSchedule3ElectronicRow(formId: Long)(row: Schedule3ElectronicRow): IO[Unit] =
    for {
      heldBy <- saveOcrField(row.heldBy)
      name <- saveOcrField(row.name)
      yearEndValue <- saveOcrField(row.yearEndValue)
      incomeType <- saveOcrField(row.incomeType)
      amount <- saveOcrField(row.amount)
      transaction <- saveOcrField(row.transaction)
      _ <- IO(db.withSession(implicit s => schedule3RowImages += ((-1, formId, heldBy, name, yearEndValue, incomeType, amount, transaction))))
    } yield ()

  private def saveOcrField(f: OcrField): IO[Long] =
    for {
      imgFile <- saveImage[IO](f.img)
      id <- IO(db.withSession(implicit s => (ocrFields returning ocrFields.map(_.id)) += ((-1, f.text, imgFile.toString, f.img.minX, f.img.minY, f.img.width, f.img.height))))
    } yield id

  def getSchedule3ElectronicById(id: Long): IO[Option[Schedule3Electronic]] =
    db.withSession(implicit s => {
      ???
    })
}