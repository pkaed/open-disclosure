package name.kaeding.opendisc
package db

import scalaz._, Scalaz._

import slick.driver._

class Tables(val profile: JdbcProfile) {
  import profile.simple._
  
  lazy val ddl: profile.SchemaDescription = 
    schedule3Images.ddl ++
    schedule3RowImages.ddl ++
    ocrFields.ddl
    
  class Schedule3ImgTable(tag: Tag) extends Table[(Long, String)](tag, "schedule_3_img") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def imgUrl = column[String]("img_url")
    
    def rows = schedule3RowImages.where(_.formId === id)
    
    def * = (id, imgUrl)
  }
  val schedule3Images = TableQuery[Schedule3ImgTable]

  class Schedule3RowImgTable(tag: Tag) extends Table[(Long, Long, Long, Long, Long, Long, Long, Long)](tag, "schedule_3_row_img") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def formId = column[Long]("form_id")
    
    def heldById = column[Long]("held_by_id")
    def nameId = column[Long]("name_id")
    def yearEndValueId = column[Long]("year_end_value_id")
    def incomeTypeId = column[Long]("income_type_id")
    def amountId = column[Long]("amount_id")
    def transactionId = column[Long]("transaction_id")
    
    def heldBy = foreignKey("held_by_fk", heldById, ocrFields)(_.id)
    def name = foreignKey("name_fk", nameId, ocrFields)(_.id)
    def yearEndValue = foreignKey("year_end_value_fk", yearEndValueId, ocrFields)(_.id)
    def incomeType = foreignKey("income_type_fk", incomeTypeId, ocrFields)(_.id)
    def amount = foreignKey("amount_fk", amountId, ocrFields)(_.id)
    def transaction = foreignKey("transaction_fk", transactionId, ocrFields)(_.id)
    
    def form = foreignKey("form_fk", formId, schedule3Images)(_.id)
    
    def * = (id, formId, heldById, nameId, yearEndValueId, incomeTypeId, amountId, transactionId)
  }
  val schedule3RowImages = TableQuery[Schedule3RowImgTable]
  
  class OcrFieldTable(tag: Tag) extends Table[(Long, Option[String], String, Int, Int, Int, Int)](tag, "ocr_fields") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def text = column[Option[String]]("text")
    def imgUrl = column[String]("img_url")
    def imgX = column[Int]("img_x")
    def imgY = column[Int]("img_y")
    def imgWidth = column[Int]("img_width")
    def imgHeight = column[Int]("img_height")
    
    def * = (id, text, imgUrl, imgX, imgY, imgWidth, imgHeight)
  }
  val ocrFields = TableQuery[OcrFieldTable]
}