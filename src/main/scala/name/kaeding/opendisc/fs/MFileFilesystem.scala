package name.kaeding.opendisc
package fs

import scalaz._, Scalaz._, effect._
import java.io._
import java.util.UUID

class MFileFilesystem(basePath: File) extends MFile[IO] {
  val ext: String = "png"
  def saveFile(writeFn: OutputStream => Unit) = 
  	for {
  	  uuid <- IO(UUID.randomUUID)
  	  file = new File(basePath, s"$uuid.$ext")
  	  _    <- IO(new FileOutputStream(file)).bracket(os => IO(os.close))(os => IO(writeFn(os)))
  	} yield file.toURI
}