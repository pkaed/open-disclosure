package name.kaeding.opendisc.img

import scala.util.control.Exception._
import java.awt.image.BufferedImage

import scalaz._, Scalaz._

import net.sourceforge.tess4j._

/**
 * Immutable wrapper around the pixels in a BufferedImage, which allows for edge-detection
 * and thread-safety.
 */
sealed case class Image(
  minX: X,
  minY: Y,
  maxX: X,
  maxY: Y,
  private val underlying: BufferedImage) {
  import Image._
  private lazy val data = underlying.getData
  lazy val height: Int = maxY - minY + 1
  lazy val width: Int = maxX - minX + 1

  def ocr: Option[String] =
    (catching(classOf[TesseractException]) opt Image.tess.doOCR(toBufferedImage)).map(_.replaceAll("[\\r\\n ]+", " ").trim())

  def subImage(top: Y, right: X, bottom: Y, left: X) = 
    Image(left, top, right, bottom, underlying)

  def contract: Image =
    (for {
      top <- findFirstWhite(rowsTopToBottom, 0.05, width)
      bottom <- findFirstWhite(rowsBottomToTop, 0.05, width)
      right <- findFirstWhite(colsRightToLeft, 0.05, height)
      left <- findFirstWhite(colsLeftToRight, 0.05, height)
    } yield subImage(top, right, bottom, left)).getOrElse(this)

  def getRow(y: Y): ImmutableArray[Pixel] = 
    ImmutableArray.fromArray(data.getPixels(minX, y, width, 1, Array.ofDim[Int](width)).asInstanceOf[Array[Pixel]])

  def getCol(x: X): ImmutableArray[Pixel] =
    ImmutableArray.fromArray(data.getPixels(x, minY, 1, height, Array.ofDim[Int](height)).asInstanceOf[Array[Pixel]])

  def toBufferedImage: BufferedImage = 
    underlying.getSubimage(minX, minY, width, height)

  /**
   * Scans inward from the right of the image, looking for a lot of black pixels.  Just how many
   * is a lot is determined by the threshold provided
   * @param threshold between 0 and 1.0, the ratio that will be the minimum to be considered a
   * border.  For instance, if you want to find a border that is 80% black pixels, pass in 0.8.
   * @return the x coordinate that has more than the threshold black pixels, or None if no such
   * column was found.
   */
  def findRightBorder(threshold: Double): Option[X] =
    findFirstBorder(colsRightToLeft, threshold, height)

  /**
   * Scans inward from the left of the image, looking for a lot of black pixels.  Just how many
   * is a lot is determined by the threshold provided
   * @param threshold between 0 and 1.0, the ratio that will be the minimum to be considered a
   * border.  For instance, if you want to find a border that is 80% black pixels, pass in 0.8.
   * @return the x coordinate that has more than the threshold black pixels, or None if no such
   * column was found.
   */
  def findLeftBorder(threshold: Double): Option[X] =
    findFirstBorder(colsLeftToRight, threshold, height)

  /**
   * Scans inward from the top of the image, looking for a lot of black pixels.  Just how many
   * is a lot is determined by the threshold provided
   * @param threshold between 0 and 1.0, the ratio that will be the minimum to be considered a
   * border.  For instance, if you want to find a border that is 80% black pixels, pass in 0.8.
   * @return the y coordinate that has more than the threshold black pixels, or None if no such
   * row was found.
   */
  def findTopBorder(threshold: Double): Option[Y] =
    findFirstBorder(rowsTopToBottom, threshold, width)

  /**
   * Scans inward from the bottom of the image, looking for a lot of black pixels.  Just how many
   * is a lot is determined by the threshold provided
   * @param threshold between 0 and 1.0, the ratio that will be the minimum to be considered a
   * border.  For instance, if you want to find a border that is 80% black pixels, pass in 0.8.
   * @return the y coordinate that has more than the threshold black pixels, or None if no such
   * row was found.
   */
  def findBottomBorder(threshold: Double): Option[Y] =
    findFirstBorder(rowsBottomToTop, threshold, width)

  def colsLeftToRight: Stream[(ImmutableArray[Pixel], X)] =
    Stream.range(minX, maxX, 1).map(X(_)).map(i => (getCol(i), i))

  def colsRightToLeft: Stream[(ImmutableArray[Pixel], X)] =
    Stream.range(maxX, minX, -1).map(X(_)).map(i => (getCol(i), i))

  def rowsTopToBottom: Stream[(ImmutableArray[Pixel], Y)] =
    Stream.range(minY, maxY, 1).map(Y(_)).map(i => (getRow(i), i))

  def rowsBottomToTop: Stream[(ImmutableArray[Pixel], Y)] =
    Stream.range(maxY, minY, -1).map(Y(_)).map(i => (getRow(i), i))

  
}

object Image {
  val tess: Tesseract = Tesseract.getInstance()
  val whiteThreshold = 0.05
  val lineThreshold = 0.4
  val maxLineWidth = 10
  val maxLineBuffer = 3
    
  def countBlackPixels(pxs: ImmutableArray[Pixel]): Int =
    (0 until pxs.length).map(pxs).count(p => (p & 0x00FFFFFF) === 0)

  def apply(underlying: BufferedImage): Image = {
    val data = underlying.getData
    val minX = X(data.getMinX)
    val minY = Y(data.getMinY)
    Image(
      minX = minX,
      minY = minY,
      maxX = X(data.getWidth + minX - 1),
      maxY = Y(data.getHeight + minY - 1),
      underlying)
  }

  def findFirstBorder[A <: Int](ipxss: Stream[(ImmutableArray[Pixel], A)], threshold: Double, totalCount: Int): Option[A] =
    findFirst(threshold, totalCount)(ipxss.map(ipxs => (Image.countBlackPixels(ipxs._1), ipxs._2)), false)

  def findFirstWhite[A <: Int](ipxss: Stream[(ImmutableArray[Pixel], A)], threshold: Double, totalCount: Int): Option[A] =
    ipxss.find(ipxs => Image.countBlackPixels(ipxs._1).toDouble / totalCount.toDouble <= threshold).map(_._2)

  def findFirst[A <: Int](threshold: Double, totalCount: Int)(s: Stream[(Int, A)], foundBlack: Boolean): Option[A] = {
    if (s.headOption.isEmpty) none
    else {
      val isBlack = s.headOption.map(_._1.toDouble / totalCount > threshold).getOrElse(true)
      if (!isBlack && foundBlack) s.head._2.some
      else findFirst(threshold, totalCount)(s.tail, isBlack)
    }
  }

  def findGridLines[A](pxs: Stream[(ImmutableArray[Pixel], A)], totalCount: Int, fuzzFactor: Int): List[A] = {
    def countBlackPixelsInGroup(lines: Stream[(ImmutableArray[Pixel], A)]): (Int, A) = {
      val index = lines(math.floor(lines.length.toFloat / 2).toInt)._2
      val rows = lines.map(_._1.toArray.toList).transpose
      val count = rows.count(r => Image.countBlackPixels(ImmutableArray.fromArray(r.toArray)) > 0)
      (count, index)
    }
    case class ScanState(
      lastFewRatios: List[(A, Double)] = Nil, 
      encounteredLines: List[A] = Nil)
    def processGroup(state: ScanState, lines: Stream[(ImmutableArray[Pixel], A)]): ScanState = {
      def isBuffer(r: (A, Double)) = r._2 > whiteThreshold && r._2 < lineThreshold
      def isLine(r: (A, Double)) = r._2 > lineThreshold
      def isWhite(r: (A, Double)) = r._2 < whiteThreshold
      val (count, index) = countBlackPixelsInGroup(lines)
      val ratio = count.toDouble / totalCount.toDouble
      val rs1 = ((index, ratio) :: state.lastFewRatios).take(maxLineWidth * 2)
      val prefix = rs1.takeWhile(isWhite)
      val rs2 = rs1.dropWhile(isWhite)
      val prefixBuffer = rs2.takeWhile(isBuffer)
      val rs3 = rs2.dropWhile(isBuffer)
      val line = rs3.takeWhile(isLine)
      val rs4 = rs3.dropWhile(isLine)
      val suffixBuffer = rs4.takeWhile(isBuffer)
      val rs5 = rs4.dropWhile(isBuffer)
      val suffix = rs5.takeWhile(isWhite)
      if (
          prefix.length > 0 && 
          prefixBuffer.length <= maxLineBuffer &&
          line.length <= (maxLineWidth + 2) && 
          line.length > 0 && 
          suffixBuffer.length <= maxLineBuffer &&
          suffix.length > 0)
        ScanState(prefix, line.headOption.map(_._1).toList ::: state.encounteredLines)
      else
        ScanState(rs1, state.encounteredLines)
    }
    pxs.sliding(fuzzFactor, 1).foldLeft(ScanState())(processGroup).encounteredLines
  }
}