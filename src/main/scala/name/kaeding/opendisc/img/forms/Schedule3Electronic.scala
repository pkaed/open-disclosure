package name.kaeding.opendisc.img
package forms

import scalaz._, Scalaz._

sealed case class Schedule3Electronic(rows: List[Schedule3ElectronicRow], img: Image) {
  def boxes: List[Image] = 
    rows.flatMap(r => List(
      r.heldBy.img, 
      r.name.img, 
      r.yearEndValue.img,
      r.incomeType.img,
      r.amount.img,
      r.transaction.img))
  def csv: String = 
    rows.map(r => s""""${r.heldBy.text.getOrElse("")}","${r.name.text.getOrElse("")}","${r.yearEndValue.text.getOrElse("")}","${r.incomeType.text.getOrElse("")}","${r.amount.text.getOrElse("")}","${r.transaction.text.getOrElse("")}" """).mkString("\n")
}
object Schedule3Electronic {
  def apply(i: Image): Option[Schedule3Electronic] = {
    val colBorders = findColumnBorders(i)
    // val tops = i.minY :: {
    //   def getNextTop(lastTop: Y): Option[Y] =
    //     i.copy(minY = lastTop).findTopBorder(0.3)
    //   def getLastBottom(nextTop: Y): Option[Y] =
    //     i.copy(maxY = nextTop).findBottomBorder(0.3)
    //   def getRemainingBorders(lastTop: Y): List[Y] = {
    //     val nextTop = getNextTop(lastTop)
    //     val tbo = for {
    //       t <- getNextTop(lastTop)
    //       b <- getLastBottom(t)
    //     } yield (t, b)
    //     tbo.cata(
    //       none = i.maxY :: Nil,
    //       some = tb =>
    //         if (tb._1 >= i.maxY)
    //           i.maxY :: Nil
    //         else
    //           tb._2 :: tb._1 :: getRemainingBorders(tb._1))
    //   }
    //   getRemainingBorders(i.minY)
    // }
    // val boxBorders: List[(Y, Y)] = 
    //   tops.sliding(2, 2).map({ case List(a, b) => (a, b) }).toList
    // val boxes = boxBorders.map(bs => i.subImage(bs._1, i.maxX, bs._2, i.minX))
    val boxes = findRowBorders(i)
    val rows: Option[List[Schedule3ElectronicRow]] = colBorders.map(cbs => boxes.map(Schedule3ElectronicRow(cbs, _)))
    rows.map(Schedule3Electronic(_, i))
  }
  
  val minColumnWidth = X(10)

  def findRowBorders(i: Image): List[Image] = {
    val ys = i.minY :: Image.findGridLines[Y](i.rowsTopToBottom, i.width, 3).reverse ::: List(i.maxY)
    val tbs = ys.sliding(2, 1).map({ case List(a, b) => (a, b) }).toList
    tbs.map((tb) => i.subImage(tb._1, i.maxX, tb._2, i.minX).contract)
  }
  
  def findColumnBorders(i: Image): Option[ColumnBorders] = {
    val xs = Image.findGridLines[X](i.colsLeftToRight, i.height, 3)
    xs.reverse match {
      case heldByRightBorder :: valueLeftBorder :: typeLeftBorder :: amountLeftBorder :: txnLeftBorder :: Nil => 
        ColumnBorders(heldByRightBorder, txnLeftBorder, amountLeftBorder, typeLeftBorder, valueLeftBorder).some
      case _ => none
    }
  }
}

sealed case class ColumnBorders(
    heldByRightBorder: X,
    txnLeftBorder: X,
    amountLeftBorder: X,
    typeLeftBorder: X,
    valueLeftBorder: X)

case class Schedule3ElectronicRow(
  heldBy: Schedule3ElectronicHeldByBox,
  name: Schedule3ElectronicNameBox,
  yearEndValue: Schedule3ElectronicYearEndValueBox,
  incomeType: Schedule3ElectronicIncomeTypeBox,
  amount: Schedule3ElectronicAmountBox,
  transaction: Schedule3ElectronicTransactionBox)
object Schedule3ElectronicRow {
  val minColumnWidth = 10
  def apply(cb: ColumnBorders, i: Image): Schedule3ElectronicRow =
    Schedule3ElectronicRow(
      heldBy = Schedule3ElectronicHeldByBox(i.subImage(i.minY, cb.heldByRightBorder, i.maxY, i.minX).contract),
      name = Schedule3ElectronicNameBox(i.subImage(i.minY, cb.valueLeftBorder, i.maxY, cb.heldByRightBorder).contract),
      yearEndValue = Schedule3ElectronicYearEndValueBox(i.subImage(i.minY, cb.typeLeftBorder, i.maxY, cb.valueLeftBorder).contract),
      incomeType = Schedule3ElectronicIncomeTypeBox(i.subImage(i.minY, cb.amountLeftBorder, i.maxY, cb.typeLeftBorder).contract),
      amount = Schedule3ElectronicAmountBox(i.subImage(i.minY, cb.txnLeftBorder, i.maxY, cb.amountLeftBorder).contract),
      transaction = Schedule3ElectronicTransactionBox(i.subImage(i.minY, i.maxX, i.maxY, cb.txnLeftBorder).contract))
}

trait OcrField {
  def img: Image
  lazy val text: Option[String] = img.ocr
}
case class Schedule3ElectronicHeldByBox(img: Image) extends OcrField
case class Schedule3ElectronicNameBox(img: Image) extends OcrField
case class Schedule3ElectronicYearEndValueBox(img: Image) extends OcrField
case class Schedule3ElectronicIncomeTypeBox(img: Image) extends OcrField
case class Schedule3ElectronicAmountBox(img: Image) extends OcrField
case class Schedule3ElectronicTransactionBox(img: Image) extends OcrField