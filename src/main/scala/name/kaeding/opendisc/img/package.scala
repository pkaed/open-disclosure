package name.kaeding.opendisc

import scalaz._, Scalaz._

package object img {
  type RasterData = Array[Array[Int]]
  trait XTag
  trait YTag
  trait PixelTag
  type X = Int// @@ XTag // SI-5183
  object X {
    def apply(x: Int): Int @@ XTag = Tag[Int, XTag](x)
  }
  type Y = Int// @@ YTag
  object Y {
    def apply(y: Int): Y = Tag[Int, YTag](y)
  }
  type Pixel = Int// @@ PixelTag
}