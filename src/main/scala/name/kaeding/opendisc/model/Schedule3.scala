package name.kaeding.opendisc
package model

import scalaz._, Scalaz._

sealed case class Schedule3(rows: List[Schedule3Row])

sealed case class Schedule3Row(
    heldBy: Option[HeldBy],
    name: Option[String],
    yearEndValue: Option[YearEndValue],
    incomeType: Option[IncomeType],
    amountofIncome: Option[AmountOfIncome],
    transaction: Option[Transaction])
    
sealed trait HeldBy
case object HeldBySpouse extends HeldBy
case object HeldJointly extends HeldBy
case object HeldByDependentChild extends HeldBy
object HeldBy {
  implicit val Shows = new Show[HeldBy] {
    override def shows(h: HeldBy) = h match {
      case HeldBySpouse => "SP"
      case HeldJointly => "JT"
      case HeldByDependentChild => "DC"
    }
  }
  val values: List[HeldBy] = 
    List(HeldBySpouse, HeldJointly, HeldByDependentChild)
}

sealed trait YearEndValue
case object Value1to1000 extends YearEndValue 
case object Value1001to15000 extends YearEndValue
case object Value15001to50000 extends YearEndValue
case object Value50001to100000 extends YearEndValue
case object Value100001to250000 extends YearEndValue
case object Value250001to500000 extends YearEndValue
case object Value500001to1000000 extends YearEndValue
case object Value1000001to5000000 extends YearEndValue
case object Value5000001to25000000 extends YearEndValue
case object Value25000001to50000000 extends YearEndValue
case object ValueOver50000000 extends YearEndValue
object YearEndValue {
  implicit val Shows = new Show[YearEndValue] {
    override def shows(v: YearEndValue) = v match {
      case Value1to1000 => "$1 - $1,000"
      case Value1001to15000 => "$1,001 - $15,000"
      case Value15001to50000 => "$15,001 - $50,000"
      case Value50001to100000 => "$50,001 - $100,000"
      case Value100001to250000 => "$100,001 - $250,000"
      case Value250001to500000 => "$250,001 - $500,000"
      case Value500001to1000000 => "$500,001 - $1,000,000"
      case Value1000001to5000000 => "$1,000,001 - $5,000,000"
      case Value5000001to25000000 => "$5,000,001 - $25,000,000"
      case Value25000001to50000000 => "$25,000,001 - $50,000,000"
      case ValueOver50000000 => "Over $50,000,000"
    }
  }
  val values: List[YearEndValue] = 
    List(Value1to1000, Value1001to15000, Value15001to50000, Value50001to100000,
        Value100001to250000, Value250001to500000, Value500001to1000000,
        Value1000001to5000000, Value5000001to25000000, Value5000001to25000000,
        Value25000001to50000000, ValueOver50000000)
}

sealed trait IncomeType
case object DividendIncome extends IncomeType
case object RentIncome extends IncomeType
case object InterestIncome extends IncomeType
case object CapitalGainsIncome extends IncomeType
case object ExceptedBlindTrustIncome extends IncomeType
case object TaxDeferredIncome extends IncomeType
case class Other(explanation: String) extends IncomeType
object IncomeType {
  implicit val Shows = new Show[IncomeType] {
    override def shows(i: IncomeType) = i match {
      case DividendIncome => "Dividends"
      case RentIncome => "Rent"
      case InterestIncome => "Interest"
      case CapitalGainsIncome => "Capital Gains"
      case ExceptedBlindTrustIncome => "Excepted/Blind Trust"
      case TaxDeferredIncome => "Tax-deferred"
      case Other(explanation) => s"Other: $explanation"
    }
  }
  val values: List[IncomeType] =
    List(DividendIncome, RentIncome, InterestIncome, CapitalGainsIncome,
        ExceptedBlindTrustIncome, TaxDeferredIncome, Other(""))
}

sealed trait AmountOfIncome
case object Income1to200 extends AmountOfIncome
case object Income201to1000 extends AmountOfIncome
case object Income1001to2500 extends AmountOfIncome
case object Income2501to5000 extends AmountOfIncome
case object Income5001to15000 extends AmountOfIncome
case object Income15001to50000 extends AmountOfIncome
case object Income50001to100000 extends AmountOfIncome
case object Income100001to1000000 extends AmountOfIncome
case object Income1000001to5000000 extends AmountOfIncome
case object IncomeOver5000000 extends AmountOfIncome
object AmountOfIncome {
  implicit val Shows = new Show[AmountOfIncome] {
    override def shows(v: AmountOfIncome) = v match {
      case Income1to200 => "$1 - $200"
      case Income201to1000 => "$201 - $1,000"
      case Income1001to2500 => "$1,001 - $2,500"
      case Income2501to5000 => "$2,501 - $5,000"
      case Income5001to15000 => "$5,001 - $15,000"
      case Income15001to50000 => "$15,001 - $50,000"
      case Income50001to100000 => "$50,001 - $100,000"
      case Income100001to1000000 => "$100,001 - $1,000,000"
      case Income1000001to5000000 => "$1,000,001 - $5,000,000"
      case IncomeOver5000000 => "Over $5,000,000"
    }
  }
  val values: List[AmountOfIncome] = 
    List(Income1to200, Income201to1000, Income1001to2500, Income2501to5000,
        Income5001to15000, Income15001to50000, Income50001to100000,
        Income100001to1000000, Income1000001to5000000, IncomeOver5000000)
}

sealed trait Transaction
case object Purchase extends Transaction
case object PurchasePartial extends Transaction
case object Sale extends Transaction
case object SalePartial extends Transaction
case object Exchange extends Transaction
case object ExchangePartial extends Transaction
object Transaction {
  implicit val Shows = new Show[Transaction] {
    override def shows(v: Transaction) = v match {
      case Purchase => "P"
      case PurchasePartial => "P (partial)"
      case Sale => "S"
      case SalePartial => "S (partial)"
      case Exchange => "E"
      case ExchangePartial => "E (partial)"
    }
  }
  val values: List[Transaction] =
    List(Purchase, PurchasePartial, Sale, SalePartial, Exchange, ExchangePartial)
}