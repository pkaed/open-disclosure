package name.kaeding.opendisc
package img

import org.specs2._
import org.scalacheck._
import Arbitrary._
import java.awt.Color

import scalaz._, Scalaz._
import scala.util.Random
import java.awt.Point
import java.awt.image._

object Generators {
  val width = 1000
  def genPixels(num: Int, color: Color): ImmutableArray[Pixel] =
    ImmutableArray.fromArray(List.fill(num)(color.getRGB).toArray)

  def genLineWithLessThanThresholdBlackPixels(t: Double): Gen[ImmutableArray[Pixel]] =
    for {
      numBlack <- Gen.chooseNum(0, (width.toDouble * t).toInt - 1)
      bs = genPixels(numBlack, Color.black)
      ws = genPixels(width - numBlack, Color.white)
      ps = Random.shuffle((bs ++ ws).toArray.toList)
    } yield ImmutableArray.fromArray(ps.toArray)

  def genLineWithMoreThanThresholdBlackPixels(t: Double): Gen[ImmutableArray[Pixel]] =
    for {
      numBlack <- Gen.chooseNum((width.toDouble * t).toInt + 1, width)
      bs = genPixels(numBlack, Color.black)
      ws = genPixels(width - numBlack, Color.white)
      ps = Random.shuffle((bs ++ ws).toArray.toList)
    } yield ImmutableArray.fromArray(ps.toArray)

  def genNel[A](g: Gen[A]) =
    for {
      h <- g
      t <- Gen.listOf(g)
    } yield h :: t

  case class ImageWithLine(
    threshold: Double,
    firstBorderStart: Int,
    firstBorderEnd: Int,
    lines: Stream[(ImmutableArray[Pixel], Int)])
    
  case class BufferedImageWithLine(
    threshold: Double,
    firstBorderStart: Int,
    firstBorderEnd: Int,
    secondBorderStart: Int,
    secondBorderEnd: Int,
    lines: List[ImmutableArray[Pixel]],
    image: BufferedImage)
    
  case class BufferedImageWithGridLines(
    gridMarkers: List[Int],
    lines: Stream[(ImmutableArray[Pixel], Int)],
    image: BufferedImage)

  case class ImageWithEdge(
    threshold: Double,
    edgeEnd: Int,
    lines: Stream[(ImmutableArray[Pixel], Int)])
    
  def toBufferedImage(lines: List[ImmutableArray[Pixel]]): BufferedImage = {
      val imgbytes: Array[Byte] = lines.map(_.toArray).toArray.flatten.map(_.toByte)
      val buffer: DataBuffer = new DataBufferByte(imgbytes, imgbytes.length)
      val thisWidth = lines.headOption.map(_.length).getOrElse(0)
      if (thisWidth * lines.length =/= imgbytes.length)
    	  println(s"thisWidth: $thisWidth, height: ${lines.length}")
      val raster = Raster.createInterleavedRaster(buffer, thisWidth, lines.length, thisWidth, 1, Array(0), new Point())
      val ret = new BufferedImage(thisWidth, lines.length, BufferedImage.TYPE_BYTE_GRAY)
      ret.setData(raster)
      ret
    }

  def genBufferedImageWithGridLines: Gen[BufferedImageWithGridLines] = {
    def buildLines(acc: Gen[(List[ImmutableArray[Pixel]], List[Int])], width: Int): Gen[(List[ImmutableArray[Pixel]], List[Int])] = 
      for {
        existing <- acc
        buf <- Gen.listOfN(width, genLineWithLessThanThresholdBlackPixels(Image.whiteThreshold))
        lineWidth  <- Gen.chooseNum(1, Image.maxLineWidth)
        line <- Gen.listOfN(lineWidth, genLineWithMoreThanThresholdBlackPixels(Image.lineThreshold))
      } yield (existing._1 ::: buf ::: line, (existing._1.length + buf.length + 1) :: existing._2)
    
    for {
      numBuffers   <- Gen.chooseNum(1, 100)
      bufferWidths <- Gen.listOfN(numBuffers, Gen.chooseNum(Image.maxLineWidth * 2, 100))
      linesMarkers <- bufferWidths.foldLeft(Gen.const((List.empty[ImmutableArray[Pixel]], List.empty[Int])))(buildLines)
      finalBuf     <- Gen.listOfN(width, genLineWithLessThanThresholdBlackPixels(Image.whiteThreshold))
      lines        = linesMarkers._1 ::: finalBuf
    } yield BufferedImageWithGridLines(
      gridMarkers = linesMarkers._2,
      lines = lines.zipWithIndex.toStream,
      image = toBufferedImage(lines))
  }

  def genBufferedImageWithLines: Gen[BufferedImageWithLine] =
    for {
      t <- Gen.chooseNum(0.0, 1.0)
      width1 <- Gen.chooseNum(1, 100)
      width2 <- Gen.chooseNum(1, 100)
      lead <- Gen.listOf(genLineWithLessThanThresholdBlackPixels(t))
      line1 <- Gen.listOfN(width1, genLineWithMoreThanThresholdBlackPixels(t))
      mid <- genNel(genLineWithLessThanThresholdBlackPixels(t))
      line2 <- Gen.listOfN(width2, genLineWithMoreThanThresholdBlackPixels(t))
      after <- genNel(genLineWithLessThanThresholdBlackPixels(t))
      ls = lead ::: line1 ::: mid ::: line2 ::: after
      firstBorderStart = lead.length
      firstBorderEnd = firstBorderStart + width1
      secondBorderStart = firstBorderEnd + mid.length
      secondBorderEnd = secondBorderStart + width2
    } yield BufferedImageWithLine(
        t, 
        firstBorderStart, 
        firstBorderEnd, 
        secondBorderStart, 
        secondBorderEnd, 
        ls.map(ia => ImmutableArray.fromArray(ia.toArray.map(0xff & _))),
        toBufferedImage(ls))
    
  def genBufferedImageWithVerticalLines: Gen[BufferedImageWithLine] =
    genBufferedImageWithLines.map(i => {
      val lines = i.lines.transpose.map(ps => ImmutableArray.fromArray(ps.toArray))
      i.copy(lines = lines, image = toBufferedImage(lines))
    })
    
  def genImageWithLine =
    for {
      t <- Gen.chooseNum(0.0, 1.0)
      width <- Gen.chooseNum(1, 100)
      lead <- Gen.listOf(genLineWithLessThanThresholdBlackPixels(t))
      line <- Gen.listOfN(width, genLineWithMoreThanThresholdBlackPixels(t))
      after <- genNel(genLineWithLessThanThresholdBlackPixels(t))
      ls = lead ::: line ::: after
    } yield ImageWithLine(t, lead.length, lead.length + width, ls.zipWithIndex.toStream)

  def genImageWithEdge =
    for {
      t <- Gen.chooseNum(0.0, 1.0)
      width <- Gen.chooseNum(0, 100)
      line <- Gen.listOfN(width, genLineWithMoreThanThresholdBlackPixels(t))
      after <- genNel(genLineWithLessThanThresholdBlackPixels(t))
      ls = line ::: after
    } yield ImageWithEdge(t, width, ls.zipWithIndex.toStream)
}