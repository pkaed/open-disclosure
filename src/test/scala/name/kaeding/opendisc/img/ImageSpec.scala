package name.kaeding.opendisc
package img

import org.specs2._
import org.scalacheck._
import Arbitrary._
import java.awt.Color

import scalaz._, Scalaz._
import scala.util.Random

class ImageSpec extends Specification with ScalaCheck {
  import Generators._
  
  def is = s2"""
    Image companion object must
      count black pixels in row                $exCountBlackPixels
      find first border                        $exFindFirstBorder 
      find first white line                    $exFindFirstWhite
      find grid lines                          $exFindGridLines
    Image must
      find top border                          $findTopBorder
      find bottom border                       $findBottomBorder
      find right border                        $findRightBorder
      find left border                         $findLeftBorder
      get rows of pixels                       $getRow
      get columns of pixels                    $getCol
                                               """

  def exFindGridLines = 
    Prop.forAll(genBufferedImageWithGridLines)(i => 
      Image.findGridLines[Int](i.lines, Generators.width, 1) must containTheSameElementsAs(i.gridMarkers, (a: Int, b: Int) => math.abs(a - b) < Image.maxLineWidth)
  )

  def exCountBlackPixels = 
    Prop.forAll(Gen.chooseNum(0, Generators.width), Gen.chooseNum(0, Generators.width))(
        (b, w) => {
    val row: ImmutableArray[Pixel] = ImmutableArray.fromArray(Random.shuffle((genPixels(b, Color.black) ++ genPixels(w, Color.white)).toArray.toList).toArray)
    Image.countBlackPixels(row) must_== b
  })
  
  def exFindFirstBorder = 
    Prop.forAll(genImageWithLine)(i => 
    	Image.findFirstBorder[Int](i.lines, i.threshold, Generators.width) must_== i.firstBorderEnd.some
  )
  
  def findTopBorder = 
    Prop.forAll(genBufferedImageWithLines)(i => 
    	Image(i.image).findTopBorder(i.threshold) must_== i.firstBorderEnd.some
  )
  
  def findBottomBorder = 
    Prop.forAll(genBufferedImageWithLines)(i => 
    	Image(i.image).findBottomBorder(i.threshold) must_== (i.secondBorderStart - 1).some
  )
  
  def findRightBorder = 
    Prop.forAll(genBufferedImageWithVerticalLines)(i => 
    	Image(i.image).findRightBorder(i.threshold) must_== (i.secondBorderStart - 1).some
  )
  
  def findLeftBorder = 
    Prop.forAll(genBufferedImageWithVerticalLines)(i => 
    	Image(i.image).findLeftBorder(i.threshold) must_== i.firstBorderEnd.some
  )
  
  def getRow = 
    Prop.forAll(genBufferedImageWithLines.flatMap(getHeight))(ih => 
    	Image(ih._1.image).getRow(ih._2).toArray must_== ih._1.lines.apply(ih._2).toArray
  )
  
  def getCol = 
    Prop.forAll(genBufferedImageWithLines.flatMap(getWidth))(ih => 
    	Image(ih._1.image).getCol(ih._2).toArray must_== ih._1.lines.map(_.apply(ih._2)).toArray
  )
  
  def exFindFirstWhite = 
    Prop.forAll(genImageWithEdge)(i => 
    	Image.findFirstWhite[Int](i.lines, i.threshold, Generators.width) must_== i.edgeEnd.some
  )
  
  private def getHeight(i: BufferedImageWithLine): Gen[(BufferedImageWithLine, Int)] =
    Gen.chooseNum(0, i.lines.length - 1).map((i, _))
    
  private def getWidth(i: BufferedImageWithLine): Gen[(BufferedImageWithLine, Int)] =
    Gen.chooseNum(0, i.lines.headOption.map(_.length - 1).getOrElse(0)).map((i, _))
}